��          \       �       �      �   u   �      #  �   &     �  �   �  �   q  c  "     �  u   �       �        �  �   �  �   b   ACF Integration Integrates the <a href="http://www.advancedcustomfields.com/">Advanced Custom Fields</a> plugin to your Customer Area No The ACF Integration add-on requires the <a href="%s">Advanced Custom Fields</a> plugin by Elliot Condon. This plugin is either missing or not activated. Yes You have installed Advanced Custom Fields %1$s. This version has not yet been tested with the ACF Integration add-on. We recommend the <a href="%2$s">latest ACF version</a>. You have installed Advanced Custom Fields %1$s. This version is known to cause problems with the ACF Integration add-on. We recommend the <a href="%2$s">latest ACF version</a>. Project-Id-Version: WP Customer Area - ACF Integration 4.0.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-12-14 11:43:19+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-22 12:06+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 אינטגרציה עם ACF Integrates the <a href="http://www.advancedcustomfields.com/">Advanced Custom Fields</a> plugin to your Customer Area לא התוסף ACF Integration מחייב את התוסף <a href="%s"> ACF </a> מאת Elliot Condon. התוסף הזה חסר או לא מופעל. כן התקנת ACF Pro % 1 $ s. גרסה זו טרם נבדקה עם התוסף ACF Integration. אנו ממליצים על <a href="%2$s"> גרסת ה- ACF האחרונה </a>. התקנת ACF Pro % 1 $ s. גרסה זו ידועה כגורמת לבעיות עם התוסף ACF Integration. אנו ממליצים על <a href="%2$s"> גרסת ה- ACF האחרונה </a>.
 