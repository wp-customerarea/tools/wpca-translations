��    )      d  ;   �      �     �     �     �     �     �     �     �     �     �     �            	   !  	   +     5  !   ;  !   ]  &     $   �     �     �     �     
  ;   !     ]     v     �     �     �     �     �               %     1     =  �   E  8   �       "   #  `  F     �
     �
     �
  	   �
     �
     �
     �
            
   !     ,     @     S     b     q  .   ~  ,   �  8   �  (        <     R     p     �  4   �     �     �          &     C     ^     ~     �     �     �     �     �  �   �  7   o     �  2   �                   "   #                      %                          '                           (      )                                    !             
                 $                 	      &       Attachments Back to files Back to pages Category Content Create file Create more Create page Details Done Edit attachments File details Next Step Next step Owner Page to create new private files. Page to create new private pages. Page to update existing private files. Page to update update private pages. Private Files - New Private Files - Update Private Pages - New Private Pages - Update Publish and edit private content from your website frontend Save and add attachments The content cannot be empty The file has been created. The file has been updated. The page has been created. The page has been updated. The title cannot be empty Title Update Update file Update page View it You are not allowed to pick an owner and there are no default owners for this type of content. Please contact your website administrator! You must save the post before you can upload attachments You must select a category You must select at least one owner Project-Id-Version: WP Customer Area - Front-office publishing 4.1.2
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2020-01-13 18:45:32+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-02-21 13:40+0100
Language-Team: http://marvinlabs.com/
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Generator: Poedit 2.2.1
Last-Translator: 
Language: pl_PL
X-Poedit-SearchPath-0: customer-area-collaboration.php
X-Poedit-SearchPath-1: src/php
 Załączniki Wróć do plików Wróć do stron Kategoria Treść Utwórz plik Stwórz więcej Utwórz stronę Szczegóły Ukończono Edytuj załączniki Informacje o pliku Następny krok Następny krok Właściciel Strona do tworzenia nowych plików prywatnych. Strona do tworzenia nowych stron prywatnych. Strona do aktualizacji istniejących plików prywatnych. Strona do aktualizacji stron prywatnych. Pliki prywatne - Nowe Pliki Prywatne - Aktualizacja Strony Prywatne - Nowa Strony prywatne - Zaktualizuj Opublikuj i zedytuj prywatne treści z widoku strony Zapisz i dodaj załączniki Należy wpisać treść Plik został utworzony. Plik został zaktualizowany. Strona została utworzona. Strona została zaktualizowana. Należy wpisać tytuł Tytuł Zaktualizuj Zaktualizuj plik Zaktualizuj stronę Zobacz Wybieranie właściciela zabronione i nie ma żadnych właścicieli domyślnych dla tego rodzaju treści. Proszę skontaktuj się z administratorem sieci! Musisz zapisać post przed załadowaniem załączników Należy wybrać kategorię Należy wybrać przynajmniej jednego właściciela 