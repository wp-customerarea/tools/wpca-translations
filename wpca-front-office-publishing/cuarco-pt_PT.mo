��    *      l      �      �     �     �     �     �     �     �                    "     '     8     E  	   ]  	   g     q  !   w  !   �  &   �  $   �               2     F  ;   ]     �     �     �     �               :     T     Z     a     m     y  �   �  8        D  "   _  �  �     
     
     1
  	   E
  	   O
     Y
  
   h
     s
     �
  	   �
     �
     �
     �
     �
     �
     �
  +   �
  *      4   K  (   �     �     �     �     �  G        a  !   |     �     �     �     �              	   (     2     E  
   W  �   b  6   �  1   5  &   g   Attachments Back to files Back to pages Category Content Create file Create more Create page Details Done Edit attachments File details Front-office publishing Next Step Next step Owner Page to create new private files. Page to create new private pages. Page to update existing private files. Page to update update private pages. Private Files - New Private Files - Update Private Pages - New Private Pages - Update Publish and edit private content from your website frontend Save and add attachments The content cannot be empty The file has been created. The file has been updated. The page has been created. The page has been updated. The title cannot be empty Title Update Update file Update page View it You are not allowed to pick an owner and there are no default owners for this type of content. Please contact your website administrator! You must save the post before you can upload attachments You must select a category You must select at least one owner Project-Id-Version: WP Customer Area - Front-office publishing 4.2.0
Report-Msgid-Bugs-To: https://gitlab.com/wp-customerarea/tools/wpca-products-build-environment/-/issues
POT-Creation-Date: 2020-11-07 14:48:07+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2021-08-22 14:39+0000
Last-Translator: Henrique
Language-Team: Português
Language: pt-PT
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: customer-area-collaboration.php
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Poedit-SearchPath-1: src/php
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.3; wp-5.8 Anexos
 Voltar aos Ficheiros Voltar às Páginas Categoria Conteúdo Criar Ficheiro Criar mais Criar página Detalhes Finalizar Editar anexos Detalhes do ficheiro Publicação front-office Próximo Passo Próximo passo Dono Página para criar novos ficheiros privados Página para criar novas páginas privadas Página para atualizar ficheiros privados existentes Página para atualizar páginas privadas Ficheiros Privados - Novo Ficheiros Privados - Atualizar Páginas privadas - Nova Páginas Privadas - Atualizar Publicar e editar conteúdo privado através do frontend do seu website Guardar e adicionar anexos O conteúdo não pode estar vazio O ficheiro foi criado. O ficheiro foi atualizado. A página foi criada. A página foi atualizada. O título náo pode estar vazio Título Atualizar Atualizar ficheiro Atualizar página Visualizar Não pode atribuir o conteúdo e não está atribuído um dono, por defeito, para este tipo de conteúdo. Por favor contacte o administrador do seu website Tem de guardar a publicação antes de submeter anexos Por favor escolha uma categoria antes de proceder Por favor atribua o ficheiro a alguém 