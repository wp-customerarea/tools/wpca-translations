��          �                         ;   "     ^     y     �     �     �     �     �     �  (   �      $  !   E  b  g  	   �     �  U   �  -   >  "   l  $   �  "   �     �     �            )   (     R     j   All %s Choose groups Create groups of customers who are managed by another user. Create/Edit managed groups Delete managed groups Edit groups managed Edit groups subscribed Managed Group Managed Groups Managers cuar_managed_groupAdd New cuar_managed_groupAdd New Managed Group cuar_managed_groupManaged Group cuar_managed_groupManaged Groups Project-Id-Version: WP Customer Area - Managed Groups 4.1.0
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-04-26 14:24:10+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-11 10:13+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 הכל %s בחר קבוצות צור קבוצות של לקוחות שמנוהלים על ידי משתמש אחר. צור / ערוך קבוצות מנוהלות מחק קבוצות מנוהלות ערוך קבוצות מנוהלות ערוך קבוצות הרשמות קבוצה מנוהלת קבוצות מנוהלות מנהלים הוסף חדש הוסף קבוצה מנוהלת חדשה קבוצה מנוהלת קבוצות מנוהלות 