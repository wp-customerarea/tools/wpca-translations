��    6      �      |      |  3   }  /   �  K   �     -     =  
   O     Z     h  #   z     �     �     �  F   �               %     ,     ;     H  &   U     |     �  &   �  +   �  )   �               7     C     R  G   f  =   �  $   �       &   /  �   V  4   �  <     :   S  4   �  2   �     �     	     	     .	     ;	  O   K	  (   �	  :   �	     �	      
     6
     E
  h  `
  =   �  <     m   D     �     �     �  	   �     �  A        P     k  	   �  f   �  
   �  '        *     /     D     X  1   l  
   �     �  <   �  0   �  ;   .     j     s     �     �  !   �  L   �  N      &   o  C   �  +   �  �     @   �  B   �  S   9  G   �  K   �  (   !     J     Z     y     �  u   �  E     O   a     �  *   �     �  "      A password will be e-mailed to you at this address. A user changed his password on your site %s: %s Allow users to login by using either their username or their email address. Change Password Create an account E-mail: %s Email Address Email or username Enter a username or e-mail address. Forgot Password Get New Password Hello, If this was a mistake, just ignore this email and nothing will happen. Login Login using email Logout Lost password? New Password New password New user registration on your site %s: Password Password is empty! Password lost and changed for user: %s Password reset is not allowed for this user Passwords do not match! Please try again. Register Registration is disabled Remember me Reset Password Reset your password Someone requested that the password be reset for the following account: Thank you for registering on our site. Your username is: %1$s The code you entered is not correct. The e-mail could not be sent. The email address isn&#8217;t correct. The username or password you entered is incorrect. <a href="%s" title="Password Lost and Found" class="alert-link">Lost your password</a>? There is no user registered with that email address. This email is already registered, please choose another one. This page allows the users to actually reset his password. To reset your password, visit the following address: To set your password, visit the following address: Type your new password again Username Username or Email Username: %s When logged-in: You can now <a href="%1$s" class="alert-link">login with your new password</a>. You can now login with your new password You must enter a valid username and a valid email address. [%1$s] Password Reset [%1$s] Password lost and changed [%1$s] Welcome [%s] New User Registration Project-Id-Version: WP Customer Area - Authentication Forms 5.1.1
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
POT-Creation-Date: 2018-09-14 14:40:10+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2019-08-15 12:06+0000
Last-Translator: ויז׳ן אנד ביונד <yoni@shtik.co.il>
Language-Team: Hebrew
Language: he_IL
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.0; wp-5.2.2 סיסמא תישלח אליך בדוא"ל בכתובת זו. משתמש שינה את הסיסמה באתר שלך %s: %s אפשר למשתמשים להתחבר באמצעות שם המשתמש או כתובת הדוא"ל שלהם. שנה סיסמא צור חשבון דוא״ל: %s דוא"ל דוא"ל או שם משתמש הזן שם משתמש או כתובת דואר אלקטרוני. שכחת את הסיסמא קבל סיסמא חדשה שלום, אם זו הייתה טעות, פשוט התעלם מהדוא"ל הזה ושום דבר לא יקרה. התחבר התחברות באמצעות דוא"ל צא שכחת סיסמא? סיסמה חדשה סיסמה חדשה רישום משתמש חדש באתר שלך  %s: סיסמא הסיסמה ריקה! הסיסמה אבדה ושונתה עבור המשתמש: %s איפוס סיסמה אסור למשתמש זה סיסמאות לא תואמות! בבקשה נסה שוב. הרשם ההרשמה מושבתת זכור אותי אפס סיסמא לאפס את הסיסמה שלך מישהו ביקש לאפס את הסיסמה עבור החשבון הבא: תודה שנרשמת לאתר שלנו. שם המשתמש שלך הוא: %1$s הקוד שהזנת אינו נכון. לא ניתן היה לשלוח את הדואר האלקטרוני. כתובת הדוא"ל אינה נכונה. שם המשתמש או הסיסמה שהזנת אינם נכונים. <a href="%s" title="Password Lost and Found" class="alert-link">איבדת את הסיסמה שלך</a>? אין משתמש רשום עם כתובת הדוא"ל ההיא. דוא"ל זה כבר רשום, אנא בחר הודעה אחרת. דף זה מאפשר למשתמשים לאפס את הסיסמה שלו בפועל. כדי לאפס את הסיסמה שלך, בקר בכתובת הבאה: כדי להגדיר את הסיסמה שלך, בקר בכתובת הבאה: הקלד שוב את הסיסמה שלך שם משתמש שם משתמש או דוא"ל שם משתמש:  %s כשאתה מחובר: כעת תוכלו <a href="%1$s" class="alert-link">להתחבר באמצעות הסיסמה החדשה שלכם</a>. כעת תוכלו להתחבר באמצעות הסיסמה החדשה עליך להזין שם משתמש חוקי וכתובת דוא"ל חוקית. שיחזור סיסמא [%1$s]  הסיסמא אבדה ושונתה [%1$s] ברוך הבא [%1$s]  רישום משתמש חדש [%s] 