��          �      |      �  4   �     &  	   /     9  
   F     Q     c     p  3   y     �     �  $   �     
                '     5  B   J     �  
   �     �  �  �  ;   N     �  
   �     �     �     �     �     �  7        K     f  #   �     �     �     �     �     �  K   �     A     M     e        	                                               
                                          Adds search capabilities to the Customer Area plugin Any type Ascending Content type Descending Last modification Max. results No match Page to search the private content owned by a user. Private Content - Search Published on %s, by %s, for %s Published on %s, by yourself, for %s Publishing date Query Search Search result Search something ... Sorry, we could not find any private content matching your search. Sort by Sort order Title Project-Id-Version: WP Customer Area - Search 3.0.3
Report-Msgid-Bugs-To: http://github.com/marvinlabs/customer-area-build-environment/issues
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-03-07 11:36+0100
Language-Team: http://marvinlabs.com/
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
X-Generator: Poedit 2.3
Last-Translator: 
Language: it
X-Poedit-SearchPath-0: .
 Añadir capacidades de búsqueda al plugin WP Customer Area Cualquier tipo Ascendente Tipo de contenido Descendente Última modificación Número máximo de resultados Ningún resultado encontrado Página para buscar el contenido privado de un usuario. Contenido privado - Buscar Publicado %s, por %s, para %s Publicado %s, por ti mismo, para %s Fecha de publicación Consulta Search Resultados de la búsqueda Buscando... Lo sentimos, no hemos encontrado ningún contenido privado en su búsqueda. Ordenar por Orden de clasificación Título 