��    #      4  /   L           	          3     :     N     Z     h     �  #   �     �     �  :   �     
            7        V  &   v  
   �     �  #   �     �  $   �       "   4     W     w  $   �  -   �  "   �  "        /     J     f  �  �     	     0	     J	     R	     _	     k	  &   x	     �	  ,   �	     �	     �	  C   �	  
   C
     N
     W
  R   [
  (   �
  (   �
             ,   "     O  !   m      �      �     �     �     �  .        N     m     �     �     �                                            #                              	                        !                         
                 "                 Access user groups Additional owner types All %s Any registered user Back-office Choose groups Create/Edit user groups Delete user groups Edit the groups from a user profile Edit user groups Global Make your private content visible to user groups and roles Members New %s Role Select users (hint: you can also type to search a user) This group now has %d member(s) This user does not belong to any group User Group User Groups View the groups from a user profile You do not belong to any group You have not yet created any groups. cuar_user_groupAdd New cuar_user_groupAdd New User Group cuar_user_groupEdit User Group cuar_user_groupNew User Group cuar_user_groupNo user groups found cuar_user_groupNo user groups found in Trash cuar_user_groupParent User Group: cuar_user_groupSearch User Groups cuar_user_groupUser Group cuar_user_groupUser Groups cuar_user_groupView User Group Project-Id-Version: Customer Area Extended Permissions
PO-Revision-Date: 2021-03-02 14:00+0100
Last-Translator: 
Language-Team: Nederlands
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.3.1
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
Report-Msgid-Bugs-To: 
X-Loco-Version: 2.5.1; wp-5.6.2
X-Poedit-SearchPath-0: ..
 Gebruikersgroepen Bijkomende gebruikertypes Alle %s Voor elk lid Back-office Kies groepen Maak/wijzig/verwijder beheerde groepen Gebruikersgroepen Wijzig de groepen van een bepaalde gebruiker Wijzig gebruikersgroepen Globaal Maak persoonlijke inhoud zichtbaar voor gebruikersgroepen en rollen Gebruikers Nieuw %s Rol Selecteer groepsleden (hint: u kunt beginnen met typen om een gebruiker te zoeken) Deze gebruikersgroep heeft %d gebruikers Deze gebruiker is geen lid van een groep Gebruikersgroep Gebruikersgroepen Bekijk de groepen van een bepaalde gebruiker U bent geen lid van een groep U heeft nog geen groepen gemaakt. Nieuwe gebruikersgroep toevoegen Nieuwe gebruikersgroep toevoegen Wijzig gebruikersgroep Nieuwe gebruikersgroep Geen gebruikersgroepen gevonden Geen gebruikersgroepen gevonden in prullenmand Bovenliggende gebruikersgroep: Zoek gebruikersgroep : %s Gebruikersgroep Gebruikersgroepen Bekijk gebruikersgroep 